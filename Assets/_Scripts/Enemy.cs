using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the enemies.
*/
public class Enemy : MonoBehaviour
{
	[SerializeField] private AudioSource source;
	[SerializeField] private AudioClip zombieGrowl;
	[SerializeField] private AudioClip zombieDie;
	[SerializeField] private int currentHealth = 3;
	private float voiceVolume = 1.0f;
	private CharMovement cm; 
	private CapsuleCollider cc;
	private BoxCollider bc;
	
	private void Start() 
	{
		source.volume = MainMenu.voicesVolume; // assign global volume level
		cm = gameObject.GetComponent<CharMovement>();
		cc = gameObject.GetComponent<CapsuleCollider>();
		bc = gameObject.GetComponent<BoxCollider>();
	}
	
	/*
		This function takes damages and destroys the enemy object.
	*/
	public void Damage(int damageAmount) 
	{
		currentHealth -= damageAmount;
		if (currentHealth <= 0) 
		{
			source.PlayOneShot(zombieDie, voiceVolume); // play death sound
			cc.enabled = false; // disable component
			bc.enabled = false; // disable component
			Destroy(transform.GetChild(0).gameObject); // destroy child objects
			if (!source.isPlaying) 
			{
				Destroy(gameObject); // destroy current game object
			}
		}
	}
	
	/*
		This function is used to trigger the movement of the enemy and
		play a sound.
	*/
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && cc.enabled == true) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(zombieGrowl, voiceVolume); // play growl sound
			}
			cm.isPlayerClose = true; // enable component
		}
	}
}