using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
	This class is used to manage the pause menu.
*/
public class PauseMenu : MonoBehaviour
{
	[SerializeField] private GameObject pausePanel;
	public static bool pauseGame = false;

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Cursor.lockState = CursorLockMode.Confined;
			Cursor.visible = true;	
			if (pauseGame == true) 
			{
				Resume();
			}
			else
			{
				Pause();
			}
		}
	}
	
	public void Resume() 
	{
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;	
		pausePanel.SetActive(false);
		Time.timeScale = 1f; // start the game
		pauseGame = false;
	}
	
	public void Pause() 
	{
		pausePanel.SetActive(true);
		Time.timeScale = 0f; // stop the game
		pauseGame = true;
	}
	
	public void Menu() 
	{
		SceneManager.LoadScene(0); // load main menu scene
	}
}