using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the ray cast.
*/
public class RayViewer : MonoBehaviour
{
	// [SerializeField] private float weaponRange = 50f; -- TO BE TESTED
	[SerializeField] private GameObject cam;
	private Camera tpsCam;
	
	void Start()
	{
		tpsCam = cam.GetComponent<Camera>();
	}

	void Update()
	{
		Vector3 lineOrigin = tpsCam.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
	}
}