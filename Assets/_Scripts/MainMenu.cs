using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/*
	This class is used to manage the main menu scene.
*/
public class MainMenu : MonoBehaviour
{
	[SerializeField] private AudioSource backgroundMusic;
	[SerializeField] private AudioSource buttonClick;
	[SerializeField] private AudioSource buttonHover;
	[SerializeField] private  Slider musicSlider;
	[SerializeField] private  Slider sfxSlider;
	[SerializeField] private  Slider voicesSlider;
	public static float musicVolume = 0.75f;
	public static float sfxVolume = 1.0f;
	public static float voicesVolume = 1.0f;
		
	// ### these variables are essential for different ranges
	// private float old_value;
	// private float old_min = 0.00f; // standard
	// private float old_max = 0.75f; // standard
	// private float new_min = 0.00f; // main background (playing mode)
	// private float new_max = 0.05f; // main background (playing mode)
	// ###
	
	private void Start() 
	{
		resetVolumes(); // reset volume level on start
		if (!backgroundMusic.isPlaying) 
		{
			backgroundMusic.Play();
		}
		Cursor.lockState = CursorLockMode.Confined;
		Cursor.visible = true;
	}
	
	private void Update() 
	{		
		musicVolume = musicSlider.value;
		sfxVolume = sfxSlider.value;
		voicesVolume = voicesSlider.value;
				
		backgroundMusic.volume = musicVolume;
		buttonClick.volume = sfxVolume;
		buttonHover.volume = sfxVolume;
	}
	
	public void PlayGame() 
	{
		SceneManager.LoadScene(1); // load the main game scene
	}
	
	public void ExitGame() 
	{
		#if UNITY_EDITOR
			UnityEditor.EditorApplication.isPlaying = false;
		#else
			Application.Quit();
		#endif
	}
	
	public void ButtonClick()
	{
		if (!buttonClick.isPlaying) 
		{
			buttonClick.Play();
		}
		else 
		{
			buttonClick.Stop();
			buttonClick.Play();
		}
	}
	
	public void ButtonHover()
	{
		if (!buttonHover.isPlaying) 
		{
			buttonHover.Play();
		}
		else 
		{
			buttonHover.Stop();
			buttonHover.Play();
		}
	}
	
	private void resetVolumes() 
	{
		if (musicVolume != 0.75f) 
		{
			musicVolume = 0.75f;
		}
		if (sfxVolume != 1.0f) 
		{
			sfxVolume = 1.0f;
		}
		if (voicesVolume != 1.0f) 
		{
			voicesVolume = 1.0f;
		}
	}

	// this function converts the current range ratio to a different range by keeping the same ratio
	// private float cutter(float value)
	// {
	// 	return (float) Math.Round((((value - old_min) / (old_max - old_min)) * (new_max - new_min) + new_min), 2);
	// }
}