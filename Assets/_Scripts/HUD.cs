using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

/*
	This class is used to manage the contents on HUD.
*/
public class HUD : MonoBehaviour
{
	[SerializeField] private GameObject hints;
	[SerializeField] private TMP_Text hintNumber;
	[SerializeField] private GameObject aim;
	[SerializeField] private GameObject instructions;
	[SerializeField] private TMP_Text instructionsMessage;
	[SerializeField] private TMP_Text objective;
	[SerializeField] private GameObject avatar;
	private int hintCount;
	
	private void Start() 
	{
		hints.SetActive(false);
	}
	
	/*
		This function updates the number of hints collected.
	*/
	public void UpdateHUDForHints(int countHints) 
	{
		if (hints.activeSelf)		
		{
			hintNumber.text = "Hints: " + countHints + "/7";
		}
		else		
		{
			hints.SetActive(true);
			hintNumber.text = "Hints: " + countHints + "/7";
		}
	}
	
	/*
		This function toggles the aiming point on the screen.
	*/
	public void ToggleAim(bool active) 
	{
		// print("toggling " + active);
		if (active == true) 
		{
		// print("toggling YES " + active);
			aim.SetActive(true);
		}
		else if (active == false) 
		{
			// print("toggling NO " + active);
			aim.SetActive(false);
		}
	}
	
	/*
		This function displays the instructive messages.
	*/
	public void UpdateHUDForInstructions(int phase) 
	{
		// print("instruction function");
		if (phase == 0) 
		{
			// print("instruction phase 0");
			instructionsMessage.text = "To run hold the left SHIFT button";
		}
		else if (phase == 1) 
		{
			instructionsMessage.text = "To jump forward press SPACE + SHIFT + W simultaneously";
		}
		else if (phase == 2) 
		{
			instructionsMessage.text = "To aim hold the RIGHT-CLICK of the mouse and shoot pressing the LEFT-CLICK. Try shoot to the legs";
		}
		else if (phase == 3) 
		{
			instructionsMessage.text = "Press T button to toggle the torch light";
		}

	}
	
	/*
		This function displays the objectives.
	*/
	public void UpdateHUDForObjective(int phase) 	
	{
		if (phase == 1) // to find the key
		{
			objective.text = "Objective: find the key";
		}
		if (phase == 2) // to unlock the gate
		{
			objective.text = "Objective: Unlock the gate";
		}
	}
	
	/*
		This function clears the instruction.
	*/
	public void clearInstructions() 
	{
		instructionsMessage.text = "";
	}
	
	/*
		This function clears the objective.
	*/
	public void clearObjective()
	
	{
		objective.text = "";
	}
}