using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the voices of the avatar.
*/
public class PlayerVoices : MonoBehaviour
{
	[SerializeField] private AudioSource source;
	[SerializeField] private AudioClip voice1;
	[SerializeField] private AudioClip voice2;
	[SerializeField] private AudioClip voice3;
	[SerializeField] private AudioClip voice4;
	[SerializeField] private AudioClip voice5;
	[SerializeField] private AudioClip voice6;
	[SerializeField] private AudioClip voice7;
	[SerializeField] private AudioClip voice8;
	[SerializeField] private AudioClip voice9;
	[SerializeField] private AudioClip voice10;
	[SerializeField] private AudioClip voice11;
	[SerializeField] private AudioClip voice12;
	[SerializeField] private AudioClip voice13;
	[SerializeField] private AudioClip suspense1;
	private float voiceVolume = 1.0f; // -- TO BE TESTED
	
	private void Start() 
	{
		source.volume = MainMenu.voicesVolume;
	}
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Voice1")
		{		
			// print("voice1");
			if (!source.isPlaying) 
			{
				// print("Playing voice1");
				source.PlayOneShot(voice1, voiceVolume);
			}			
		}
		else if (other.gameObject.tag == "Voice4")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice4, voiceVolume);
			}		
		}
		else if (other.gameObject.tag == "Voice6")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice6, voiceVolume);
				Destroy(other.gameObject);
			}		
		}
		else if (other.gameObject.tag == "Voice7")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice7, voiceVolume);
			}		
		}
		else if (other.gameObject.tag == "Voice8")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice8, voiceVolume);
			}		
		}
		else if (other.gameObject.tag == "Voice9")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice9, voiceVolume);
			}		
		}
		else if (other.gameObject.tag == "Voice12")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice12, voiceVolume);
			}		
		}
		else if (other.gameObject.tag == "suspense1")
		{		
			if (!source.isPlaying) 
			{
				source.PlayOneShot(suspense1, voiceVolume);
				Destroy(other.gameObject); // destroy the object trigger
			}		
		}
	}
	
	/*
		This function plays voices that are related to the hints.
	*/
	public void PlayVoiceHints(int hintNumber)
	{
		if (hintNumber == 1) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice2, voiceVolume);
			}
		}
		if (hintNumber == 4) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice10, voiceVolume);
			}
		}
		if (hintNumber == 7) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice13, voiceVolume);
			}
		}
	}
	
	/*
		This function plays voices related to the guns.
	*/
	public void PlayVoiceWeapons(int weaponNumber)
	{
		if (weaponNumber == 1) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice3, voiceVolume);
			}
		}
		if (weaponNumber == 2) 
		{
			if (!source.isPlaying) 
			{
				source.PlayOneShot(voice5, voiceVolume);
			}
		}
	}
	
	/*
		This function plays a voice related to the collection of the artefact.
	*/
	public void PlayVoiceArtefact()
	{
		if (!source.isPlaying) 
		{
			source.PlayOneShot(voice11, voiceVolume);
		}
	}
}