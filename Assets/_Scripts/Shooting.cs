using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the shooting.
*/
public class Shooting : MonoBehaviour
{
	[SerializeField] private int gunDamage = 1;
	[SerializeField] private float fireRate = 0.25f;
	[SerializeField] private float weaponRange = 50f;
	[SerializeField] private float hitForce = 100f;
	[SerializeField] private Transform gunEnd;
	[SerializeField] private AudioSource gunShot;	
	[SerializeField] private GameObject cam;
	private Camera tpsCam;
	private WaitForSeconds shotDuration = new WaitForSeconds(.07f);
	private LineRenderer shotLine;
	private float nextFire;
	
	// Start is called before the first frame update
	void Start()
	{
		shotLine = GetComponent<LineRenderer>();
		tpsCam = cam.GetComponent<Camera>();
		gunShot.volume = MainMenu.sfxVolume;
	}

	void Update()
	{ 
		if (Input.GetKeyDown(KeyCode.Mouse0) && Time.time > nextFire && Player.isShooting == true && Weapons.countWeapons == 2) 
		{
			// print("Shooting");
			nextFire = Time.time + fireRate;
			if (!gunShot.isPlaying) 
			{
				gunShot.Play();
			}
			else 
			{
				gunShot.Stop();
				gunShot.Play();
			}
			StartCoroutine(ShotEffect());
			
			Vector3 rayOrigin = tpsCam.ScreenToWorldPoint (new Vector3(0.5f, 0.5f, 0));
			
			RaycastHit hit;
			
			shotLine.SetPosition(0, gunEnd.position);
			
			if (Physics.Raycast (rayOrigin, tpsCam.transform.forward, out hit, weaponRange)) 
			{
				shotLine.SetPosition(1, hit.point);
				if (hit.collider.gameObject.tag == "spider1") 
				{
					// print("detected");
					Destroy(hit.collider);
				}
				else 
				{				
					Enemy enemy = hit.collider.GetComponent<Enemy>(); // intersected assign it
					
					if (enemy != null) 
					{
						enemy.Damage (gunDamage);
					}
					
					if (hit.rigidbody != null) 
					{
						hit.rigidbody.AddForce(-hit.normal * hitForce);
					}
				}

			}
			else 
			{
				shotLine.SetPosition(1, rayOrigin + (tpsCam.transform.forward * weaponRange));
			}
		}
	}
	
	private IEnumerator ShotEffect() 
	{
		shotLine.enabled = true;
		yield return shotDuration;
		shotLine.enabled = false;
	}
}