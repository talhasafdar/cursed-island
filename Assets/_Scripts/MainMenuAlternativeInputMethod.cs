using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/*
	This class is used to provide alternative input to navigate 
	the main menu.
*/
public class MainMenuAlternativeInputMethod : MonoBehaviour
{
	[SerializeField] private EventSystem eventSystem;
	[SerializeField] private GameObject selectedObject;
	private bool buttonSelected;

	void Update()
	{
		// if any movement on vertical axis
		if (Input.GetAxisRaw("Vertical") != 0 && buttonSelected == false)
		{
			eventSystem.SetSelectedGameObject(selectedObject); // select
			buttonSelected = true;
		}
	}

	private void OnDisable() 
	{
		buttonSelected = false;
	}
}