using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the riddle for opening the gate.
*/
public class RiddleGate : MonoBehaviour
{	
	[SerializeField] private AudioSource gateOpening;
	[SerializeField] private GameObject canvas;
	[SerializeField] private GameObject riddleHintMessage;
	[SerializeField] private GameObject riddleOpenHelpMessage;
	[SerializeField] private GameObject openGate;
	private bool audioPlayed = false; // after audio destroy
	[SerializeField] private GameObject riddleKey; // initially disabled enabled if go towards the gate
	[SerializeField] private GameObject voice8Trigger;
	[SerializeField] private GameObject voice9Trigger;
	
	private void Start() 
	{
		gateOpening.volume = MainMenu.sfxVolume;	
	}
	
	public void OpenGate(int RiddleKeyCollected) 
	{
		if (RiddleKeyCollected == 2) 
		{
			// print("gate opened");
			if (!gateOpening.isPlaying)
			{
				audioPlayed = true;
				gateOpening.Play();
			}
		}
	}
	
	private void Update() 
	{
		if (!gateOpening.isPlaying && audioPlayed == true) 
		{
			openGate.SetActive(true);
			Destroy(gameObject);
			canvas.SendMessage("clearObjective");
		}
	}
	
	public void ShowMessage(int RiddleKeyCollected) 
	{
		// Debug.Log("key is: " + RiddleKeyCollected);
		if (RiddleKeyCollected == 0) // if still needs to be collected 
		{
			if (riddleHintMessage != null) 
			{
				if (!voice8Trigger.activeSelf && !voice9Trigger.activeSelf) 
				{
					voice8Trigger.SetActive(true);
					voice9Trigger.SetActive(true);
				}	
				if (!riddleKey.activeSelf) 
				{
					riddleKey.SetActive(true);
				}
				canvas.SendMessage("UpdateHUDForObjective", 1);
				riddleHintMessage.SetActive(true);
			}
		}
		else if (RiddleKeyCollected == 2) 
		{
			if (riddleHintMessage != null) 
			{
				riddleOpenHelpMessage.SetActive(true);
			}
		}
	}
	
	public void HideMessage(int RiddleKeyCollected) 
	{
		if (RiddleKeyCollected == 0) // if still needs to be collected 
		{
			if (riddleHintMessage != null) 
			{
				riddleHintMessage.SetActive(false);
			}
		}
		else if (RiddleKeyCollected == 2) 
		{
			if (riddleHintMessage != null) 
			{
				riddleOpenHelpMessage.SetActive(false);
			}
		}
	}
}