using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to reposition the player to the starting point
	when it fails a challenge.
*/
public class PlayerRetry : MonoBehaviour
{
	[SerializeField] private int obstacleNumber;
	[SerializeField] private Canvas canvas;
	[SerializeField] private GameObject liftRock; // to reset its position
	private float delayTime = 0.5f;
	
	// void Start()
	// {
	// 	print("initial pos: " + transform.position);	
	// 	print("initial rot: " + transform.rotation);	
	// }
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "obstacle0") 
		{	
			StartCoroutine(ObstacleZero(delayTime));
		}		
		else if (other.gameObject.tag == "obstacle1") 
		{
			StartCoroutine(ObstacleOne(delayTime));
		}		
		else if (other.gameObject.tag == "obstacle2") 
		{	
			StartCoroutine(ObstacleTwo(delayTime));
		}
		else if (other.gameObject.tag == "obstacle3") 
		{	
			StartCoroutine(ObstacleThree(delayTime));
		}
		else if (other.gameObject.tag == "obstacle4") 
		{
			StartCoroutine(ObstacleFour(delayTime));
		}
	}
	
	/*
		This function works as a backup if the player is not respawn 
		on trigger enter.
	*/
	private void OnTriggerStay(Collider other) 
	{
		if (other.gameObject.tag == "obstacle0") 
		{
			if (other.gameObject.transform.position.x != 519.49f || 
			other.gameObject.transform.position.y != 31.04f	|| 
			other.gameObject.transform.position.z != 153.19f)				
			{
				StartCoroutine(ObstacleZero(delayTime));
			}
		}	
		else if (other.gameObject.tag == "obstacle1") 
		{
			if (other.gameObject.transform.position.x != 729.95f || 
			other.gameObject.transform.position.y != 31.04f	|| 
			other.gameObject.transform.position.z != 160.23f)				
			{
				StartCoroutine(ObstacleOne(delayTime));
			}
		}	
		else if (other.gameObject.tag == "obstacle2") 
		{
			if (other.gameObject.transform.position.x != 712.98f || 
			other.gameObject.transform.position.y != 30.95f	|| 
			other.gameObject.transform.position.z != 692.30f)				
			{
				StartCoroutine(ObstacleTwo(delayTime));
			}
		}	
		else if (other.gameObject.tag == "obstacle3") 
		{
			if (other.gameObject.transform.position.x != 283.29f || 
			other.gameObject.transform.position.y != 31.98f	|| 
			other.gameObject.transform.position.z != 824.65f)				
			{
				StartCoroutine(ObstacleThree(delayTime));
			}
		}	
		else if (other.gameObject.tag == "obstacle4") 
		{
			if (other.gameObject.transform.position.x != 326.19f || 
			other.gameObject.transform.position.y != 31.98f	|| 
			other.gameObject.transform.position.z != 831.40f)				
			{
				StartCoroutine(ObstacleFour(delayTime));
			}
		}	
	}
	
	IEnumerator ObstacleZero(float delayTime)
	{
		// print("TRIGGERED OB 0");
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		transform.position = new Vector3(519.49f, 31.04f, 153.19f);	
		// transform.Rotate(0.00000f, -0.71834f, -0.69569f);
	}
	
	IEnumerator ObstacleOne(float delayTime)
	{
		// print("TRIGGERED OB 1");
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		transform.position = new Vector3(729.95f, 31.04f, 160.23f);	
	}
	IEnumerator ObstacleTwo(float delayTime)
	{
		// print("TRIGGERED OB 2");
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		transform.position = new Vector3(712.98f, 30.95f, 692.30f);	
	}
	IEnumerator ObstacleThree(float delayTime)
	{
		// print("TRIGGERED OB 3");
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		transform.position = new Vector3(283.29f, 31.98f, 824.65f);	
		transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
	}
	IEnumerator ObstacleFour(float delayTime)
	{
		// print("TRIGGERED OB 4");
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		transform.position = new Vector3(326.19f, 31.98f, 831.40f);	
		liftRock.transform.position = new Vector3(349.45001220703127f, 26.0f, 830.2000122070313f);
		FifthRock.liftUp = false;
		FifthRock.limitTime = 0;	
	}
}