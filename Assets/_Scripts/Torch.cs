using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
	This class is used to manage the collection of the torch.
*/
public class Torch : MonoBehaviour
{
	[SerializeField] private GameObject torchMessage;
	[SerializeField] private GameObject collectedTorch;
	[SerializeField] private GameObject voice12Trigger;
	[SerializeField] private GameObject phase3; // instruction
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && torchMessage != null) 
		{
			torchMessage.SetActive(true);
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (torchMessage != null)		
		{
			torchMessage.SetActive(false);
		}
	}
	
	public void CollectTorch() 
	{
		if (voice12Trigger.activeSelf) 
		{
			Destroy(voice12Trigger);
		}
		collectedTorch.SetActive(true);
		phase3.SetActive(true);
		Destroy(gameObject);
	}
}