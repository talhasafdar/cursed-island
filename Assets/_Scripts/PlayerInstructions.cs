using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage instructional texts.
	It sends messages to HUD class.
*/
public class PlayerInstructions : MonoBehaviour
{
	[SerializeField] private GameObject canvas;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "phase0")
		{
			// print("first instruction");
			int phase = 0;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase1")
		{
			int phase = 1;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase2")
		{
			int phase = 2;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase3")
		{
			int phase = 3;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
	}
	
	private void OnTriggerStay(Collider other) 
	{
		if (other.gameObject.tag == "phase0")
		{
			// print("first instruction");
			int phase = 0;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase1")
		{
			int phase = 1;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase2")
		{
			int phase = 2;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
		else if (other.gameObject.tag == "phase3")
		{
			int phase = 3;
			canvas.SendMessage("UpdateHUDForInstructions", phase);
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (other.gameObject.tag == "phase0")
		{
			canvas.SendMessage("clearInstructions");
		}
		else if (other.gameObject.tag == "phase1")
		{
			canvas.SendMessage("clearInstructions");
		}
		else if (other.gameObject.tag == "phase2")
		{
			canvas.SendMessage("clearInstructions");
		}
		else if (other.gameObject.tag == "phase3")
		{
			canvas.SendMessage("clearInstructions");
		}
	}
}