using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the riddle key.
*/
public class RiddleKey : MonoBehaviour
{
	[SerializeField] private GameObject riddleHintMessage;
	[SerializeField] private AudioClip keyObtained;
	private GameObject playerLoad; // for audio holder since it remains
	[SerializeField] private GameObject canvas; // for objectives
	[SerializeField] private GameObject destroyParent;
	[SerializeField] private GameObject voice7;
	[SerializeField] private GameObject voice8;
	[SerializeField] private GameObject voice9;
	
	void Update()
	{
		transform.Rotate(new Vector3(0, 50 * Time.deltaTime, 0));
	}
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && riddleHintMessage != null) 
		{
			playerLoad = other.gameObject;
			riddleHintMessage.SetActive(true);
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (riddleHintMessage != null)		
		{
			riddleHintMessage.SetActive(false);
		}
	}
	
	public void CollectRiddleKey() 
	{
		AudioSource.PlayClipAtPoint(keyObtained, playerLoad.transform.position, MainMenu.sfxVolume);
		if (voice7.activeSelf) 
		{
			Destroy(voice7);
		}
		if (voice8.activeSelf) 
		{
			Destroy(voice8);
		}
		if (voice9.activeSelf) 
		{
			Destroy(voice9);
		}
		Destroy(destroyParent);
		canvas.SendMessage("UpdateHUDForObjective", 2);
	}
}