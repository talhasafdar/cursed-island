using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the hints.
*/
public class Hints : MonoBehaviour
{
	[SerializeField] private GameObject parent; // for audio holder since it remains
	[SerializeField] private GameObject hintMessage;
	[SerializeField] private GameObject spotLight;
	[SerializeField] private AudioClip hintObtained; // volume not to be affected as is part of essential immersive experience
	
	/*
		This function enables the hint message when player collides.
	*/
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && hintMessage != null) 
		{
			hintMessage.SetActive(true);
		}
	}
	
	/*
		This function disables the hint message when player collides.
	*/
	private void OnTriggerExit(Collider other) 
	{
		if (hintMessage != null)		
		{
			hintMessage.SetActive(false);
		}
	}
	
	/*
		This function destroys the hint and along relevant objects.
	*/
	public void CollectLetter() 
	{
		AudioSource.PlayClipAtPoint(hintObtained, parent.transform.position, MainMenu.sfxVolume);
		Destroy(gameObject);
		Destroy(hintMessage);
		Destroy(spotLight);
	}
}