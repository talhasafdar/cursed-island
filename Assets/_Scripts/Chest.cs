using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the vault for the ancient artefact.
	It takes different fields to open the vault and allow to collect
	the artefact.
*/
public class Chest : MonoBehaviour
{
	[SerializeField] private AudioSource chestOpening;
	[SerializeField] private GameObject chestHintMessage; // the hint message for the vault [0]
	[SerializeField] private GameObject artefactHintMessage; // the hint message for collecting the artefact [1]
	[SerializeField] private GameObject artefact;
	[SerializeField] private GameObject openChest; // the open vault game object
	[SerializeField] private GameObject pointLight; // the light to illuminate the artefact
	[SerializeField] private GameObject allowForward; // the wall that blocks going forward
	private bool audioPlayed = false; // after audio destroy
	
	private void Start() 
	{
		chestOpening.volume = MainMenu.sfxVolume;	
	}
		
	/*
		This function takes one argument to show the hint messages.
	*/
	public void ShowMessage(bool chestOpened) 
	{
		if (chestOpened == false) // if vault close show vault hint message [0]
		{
			if (chestHintMessage != null) 
			{
				chestHintMessage.SetActive(true);
			}
		}
		else if (chestOpened == true) // if vault open show artefact hint message [1]
		{
			if (artefactHintMessage != null) 
			{
				artefactHintMessage.SetActive(true);
			}
		}
	}
	
	/*
		This function takes one argument to hide the hint messages.
	*/
	public void HideMessage(bool chestOpened) 
	{
		if (chestOpened == false) // if vault close hide vault hint message [0]
		{
			if (chestHintMessage != null) 
			{
				chestHintMessage.SetActive(false);
			}
		}
		else if (chestOpened == true) // if vault open hide artefact hint message [1]
		{
			if (artefactHintMessage != null) 
			{
				artefactHintMessage.SetActive(false);
			}
		}
	}
	
	/*
		This function with one argument destroys the current game object and enables the open vault object.
	*/
	public void OpenChest(bool chestOpened) 
	{
		if (chestOpened == true) 
		{
			if (!chestOpening.isPlaying)
			{
				audioPlayed = true;
				chestOpening.Play();
			}
		}
	}
	
	private void Update() 
	{
		if (!chestOpening.isPlaying && audioPlayed == true) 
		{
			openChest.SetActive(true);
			Destroy(gameObject);
		}
	}
	
	/*
		This function with one argument destroys game objects.
	*/
	public void CollectArtefact(bool artefactCollected) 
	{
		// print("artefact Collected");
		if (artefactCollected == true) 
		{
			Destroy(chestHintMessage);
			Destroy(artefactHintMessage);
			Destroy(artefact);
			Destroy(pointLight);
			Destroy(allowForward); // to let the user move forward
		}
	}
}