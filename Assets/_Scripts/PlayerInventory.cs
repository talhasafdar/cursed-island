using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
	This class is used to manage the inventory.
*/
public class PlayerInventory : MonoBehaviour
{
	public static int countHints = 0; // for sharing with voices
	public static int countGuns = 0;
	private bool rightHandGun = false;
	private bool leftHandGun = false;	
	private bool collectableLetter = false;
	private bool collectableWeapon = false;
	private int riddleKeyCollected = 0; // 0=no, 1=yesForCollected, 2=yesForOpenGate
	private int chestKeyCollected = 0; // 0=no, 1=yesForCollected, 2=yesForOpenVault
	private bool torchCollected = false;
	private bool chestOpened = false;
	private bool artefactCollected = false;
	private bool canPlaceArtefact = false;
	private bool toggleTorch = false;
	private GameObject otherGameObject;	
	[SerializeField] private GameObject player;	
	[SerializeField] private Transform rightGun;
	[SerializeField] private Transform leftGun;		
	[SerializeField] private GameObject canvas;
	[SerializeField] private GameObject rain;
	[SerializeField] private GameObject torch;
		
	void Start()
	{		
		rightGun.GetChild(0).gameObject.SetActive(false);
		leftGun.GetChild(0).gameObject.SetActive(false);
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.C) && collectableLetter == true)			
		{
			if (countHints != 8 && countHints < 8) 			
			{
				// print("collected");	
				collectableLetter = false; // final false since the object gets destroyed cant use ontriggerexit
				otherGameObject.SendMessage("CollectLetter");
				countHints = countHints + 1;
				canvas.SendMessage("UpdateHUDForHints", countHints);
				gameObject.SendMessage("PlayVoiceHints", countHints);
			}
		}
		else if(Input.GetKeyDown(KeyCode.C) && collectableWeapon == true)			
		{
			// print("else if weapon");
			if (rightHandGun != true || leftHandGun != true)			
			{
				if (rightHandGun != true)				
				{
					// print("right");
					rightHandGun = true;
					rightGun.GetChild(0).gameObject.SetActive(true);
					countGuns = countGuns + 1;
					gameObject.SendMessage("PlayVoiceWeapons", countGuns);
				}
				else if (leftHandGun != true) 
				{
					// print("left");
					leftHandGun = true;
					leftGun.GetChild(0).gameObject.SetActive(true);
					countGuns = countGuns + 1;
					gameObject.SendMessage("PlayVoiceWeapons", countGuns);
				}
				// print("collected the weapon");
				collectableWeapon = false;
				otherGameObject.SendMessage("CollectWeapon");
			}

		}
		else if (Input.GetKeyDown(KeyCode.C) && riddleKeyCollected == 1)			
		{
			// print("riddle key collected");	
			otherGameObject.SendMessage("CollectRiddleKey");
			riddleKeyCollected = 2;
		}		
		else if (Input.GetKeyDown(KeyCode.U) && riddleKeyCollected == 2)
		{
			// print("gate unlocked");	
			otherGameObject.SendMessage("OpenGate", riddleKeyCollected); 
		}
		else if (Input.GetKeyDown(KeyCode.C) && chestKeyCollected == 1)			
		{
			// print("collected hint and key");	
			otherGameObject.SendMessage("CollectLetter"); // for hint and key for the artefact
			countHints = countHints + 1;
			canvas.SendMessage("UpdateHUDForHints", countHints);
			chestKeyCollected = 2;
			gameObject.SendMessage("PlayVoiceHints", countHints);
		}	
		else if (Input.GetKeyDown(KeyCode.O) && chestOpened == true && chestKeyCollected == 2) 
		{
			// print("chest opened");
			otherGameObject.SendMessage("OpenChest", chestOpened);
		}
		else if (Input.GetKeyDown(KeyCode.C) && artefactCollected == true && chestKeyCollected == 2) 
		{
			// print("artefact collected");
			canPlaceArtefact = false;
			otherGameObject.SendMessage("CollectArtefact", artefactCollected);
			gameObject.SendMessage("PlayVoiceArtefact");
			artefactCollected = false;
		}
		else if (Input.GetKeyDown(KeyCode.C) && torchCollected == true) 
		{
			// print("torch collected");
			otherGameObject.SendMessage("CollectTorch");
			torchCollected = false;
			toggleTorch = true;
		}
		else if (Input.GetKeyDown(KeyCode.T) && toggleTorch == true) 
		{
			// print("torch turned off");
			if (torch.activeSelf) 
			{
				torch.SetActive(false);
			}
			else if (!torch.activeSelf) 
			{
				torch.SetActive(true);
			}
		}
		else if (Input.GetKeyDown(KeyCode.P) && canPlaceArtefact == true) 
		{
			otherGameObject.SendMessage("PlaceArtefact");
			canPlaceArtefact = false;
			EndGame(1);
		}
	}	
		
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Letter") 
		{
			// print("can collect");
			collectableLetter = true;			
			otherGameObject = other.gameObject;
		}
		else if (other.gameObject.tag == "Weapon")		
		{
			// print("can collect the weapon");
			collectableWeapon = true;
			otherGameObject = other.gameObject; // reusable variable otherGameObject for sending messages in update
		}
		else if (other.gameObject.tag == "RiddleGate") 
		{
			// print("can I open this gate?");
			otherGameObject = other.gameObject;
			otherGameObject.SendMessage("ShowMessage", riddleKeyCollected);
		}
		else if (other.gameObject.tag == "RiddleKey") 
		{
			// print("key collected");
			riddleKeyCollected = 1; // add checks to increase security
			otherGameObject = other.gameObject;
		}
		else if (other.gameObject.tag == "HintAndKey") 
		{
			// Debug.Log("Hint and Key collected");
			chestKeyCollected = 1;
			otherGameObject = other.gameObject;
		}
		else if (other.gameObject.tag == "Chest") 
		{
			// Debug.Log("Can open chest");
			otherGameObject = other.gameObject;
			otherGameObject.SendMessage("ShowMessage", chestOpened);
			chestOpened = true;
		}
		else if (other.gameObject.tag == "ChestOpen") 
		{
			// Debug.Log("can collect artefact");
			artefactCollected = true;
			otherGameObject = other.gameObject;
			otherGameObject.SendMessage("ShowMessage", chestOpened);
		}
		else if (other.gameObject.tag == "Torch") 
		{
			// Debug.Log("can collect torch");
			// print("torch collectable");
			torchCollected = true;
			otherGameObject = other.gameObject;
			torchCollected = true;
		}
		else if (other.gameObject.tag == "AncientArtefact") 
		{
			if (torch.activeSelf) 
			{
				torch.SetActive(false);
			}
			// print("Artefact");
			otherGameObject = other.gameObject;
			canPlaceArtefact = true;
			// Debug.Log("TZ: " + artefactCollected);
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (other.gameObject.tag == "Letter") 
		{
			// print("can't collect");
			collectableLetter = false; // valid only if not collected
		}
		else if (other.gameObject.tag == "Weapon") 
		{
			// print("can't collect the weapon");
			collectableWeapon = false; // valid only if not collected
		}
		else if (other.gameObject.tag == "RiddleGate") 
		{
			// print("bye by gate");
			otherGameObject.SendMessage("HideMessage", riddleKeyCollected);
		}
		else if (other.gameObject.tag == "RiddleKey") 
		{
			// print("can't collect the key");
			riddleKeyCollected = 0;
		}
		else if (other.gameObject.tag == "HintAndKey") 
		{
			// print("can't collect the hint and key");
			chestKeyCollected = 0;
		}
		else if (other.gameObject.tag == "Chest") 
		{
			// print("can't open the chest");
			chestOpened = false;
			otherGameObject.SendMessage("HideMessage", chestOpened);
		}
		else if (other.gameObject.tag == "ChestOpen") 
		{
			// print("can't open the chest");
			chestOpened = false;
			otherGameObject.SendMessage("HideMessage", chestOpened);
		}
		else if (other.gameObject.tag == "Torch") 
		{
			// Debug.Log("can't collect torch");
			torchCollected = false;
		}
		else if (other.gameObject.tag == "AncientArtefact") 
		{
			// print("Artefact");
			canPlaceArtefact = false;
		}
	}
	
	IEnumerator EndGame(float delayTime)
	{
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(delayTime);
		SceneManager.LoadScene(0);
	}
}