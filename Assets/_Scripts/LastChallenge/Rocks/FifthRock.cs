using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to enable the current rock.
*/
public class FifthRock : MonoBehaviour
{
	public static bool liftUp = false;		
	public static int limitTime = 0;
	
	private void FixedUpdate() 
	{
		if (limitTime <= 200 && liftUp == true) 
		{
			transform.Translate(new Vector3(0, 1 * Time.deltaTime, 0)); // move object perpendicularly upward
			limitTime = limitTime +1;
		}
	}
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			// print("true for lift");
			liftUp = true;
		}	
	}
}