using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to enable the next rock.
*/
public class ThirdRock : MonoBehaviour
{
	[SerializeField] private AudioClip stoneSound;
	[SerializeField] private GameObject fourthRock;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			AudioSource.PlayClipAtPoint(stoneSound, fourthRock.transform.position, MainMenu.sfxVolume);
			fourthRock.SetActive(true);
		}
	}
}