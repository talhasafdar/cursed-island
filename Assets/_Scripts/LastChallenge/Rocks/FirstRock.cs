using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to enable the next rock.
*/
public class FirstRock : MonoBehaviour
{
	[SerializeField] private AudioClip stoneSound;
	[SerializeField] private GameObject secondRock;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			AudioSource.PlayClipAtPoint(stoneSound, secondRock.transform.position, MainMenu.sfxVolume);
			secondRock.SetActive(true);
		}
	}
}