using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to enable the next rock.
*/
public class SecondRock : MonoBehaviour
{
	[SerializeField] private AudioClip stoneSound;
	[SerializeField] private GameObject thirdRock;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			AudioSource.PlayClipAtPoint(stoneSound, thirdRock.transform.position, MainMenu.sfxVolume);
			thirdRock.SetActive(true);
		}
	}
}