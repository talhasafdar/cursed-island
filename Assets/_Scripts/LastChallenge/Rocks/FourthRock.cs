using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to enable the next rock.
*/
public class FourthRock : MonoBehaviour
{
	[SerializeField] private AudioClip stoneSound;
	[SerializeField] private GameObject fifthRock;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			AudioSource.PlayClipAtPoint(stoneSound, fifthRock.transform.position, MainMenu.sfxVolume);
			fifthRock.SetActive(true);
		}	
	}
}