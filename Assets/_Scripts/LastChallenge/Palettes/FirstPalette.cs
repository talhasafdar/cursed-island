using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used for animation.
*/
public class FirstPalette : MonoBehaviour
{	
	private Vector3 palette1 = new Vector3(290.777008f, 29.6499996f, 827.580017f); // east
	private Vector3 palette11 = new Vector3(290.777008f, 29.6499996f, 819.349976f); // west	
	[SerializeField] private float speed = 0.3f;

	void FixedUpdate()
	{
		if (gameObject.name == "Palette1") 
		{
			gameObject.transform.position = Vector3.Lerp(palette1, palette11, Mathf.PingPong(Time.time * speed, 1));
		}
	}
}