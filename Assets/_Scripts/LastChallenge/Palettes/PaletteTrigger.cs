using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to make the player the child object.
*/
public class PaletteTrigger : MonoBehaviour
{	
	private GameObject player;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			other.gameObject.transform.SetParent(transform, true);
		}	
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			other.gameObject.transform.SetParent(null);
		}	
	}
}