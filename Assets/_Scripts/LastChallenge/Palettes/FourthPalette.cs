using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used for animation.
*/
public class FourthPalette : MonoBehaviour
{
	[SerializeField] private AudioClip stoneSound;
	[SerializeField] private GameObject firstRock;
	[SerializeField] private GameObject obstacle3ToRemove; // to remove obstacle 3 for first part of respawn
	[SerializeField] private GameObject obstacle4; // current obstacle for respawn
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player") 
		{
			if (obstacle3ToRemove != null) 
			{
				Destroy(obstacle3ToRemove);
			}
			if (!obstacle4.activeSelf) 
			{
				obstacle4.SetActive(true);
			}
			other.gameObject.transform.localScale = new Vector3(1.0f, 1.0f, 1.0f); // reset avatar scale size
			AudioSource.PlayClipAtPoint(stoneSound, firstRock.transform.position, MainMenu.sfxVolume);
			firstRock.SetActive(true);
		}	
	}
}