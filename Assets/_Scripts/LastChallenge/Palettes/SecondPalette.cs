using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used for animation.
*/
public class SecondPalette : MonoBehaviour
{
	private Vector3 palette2 = new Vector3(294.37f, 28f, 815.90f); // east
	private Vector3 palette22 = new Vector3(294.37f, 28f, 829.72f); // west	
	[SerializeField] private float speed = 0.3f;

	void FixedUpdate()
	{
		gameObject.transform.position = Vector3.Lerp(palette2, palette22, Mathf.PingPong(Time.time * speed, 1));
	}
}