using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used for animation.
*/
public class ThirdPalette : MonoBehaviour
{
	[SerializeField] private  Vector3 palette3 = new Vector3(300f, 27f, 825f); // east
	[SerializeField] private Vector3 palette33 = new Vector3(320f, 30f, 825f); // west
	[SerializeField] private bool animate = true;
	private float speed = 0.2f;

	void FixedUpdate()
	{
		if (animate == true) 
		{
			gameObject.transform.position = Vector3.Lerp(palette3, palette33, Mathf.PingPong(Time.time * speed, 1));
		}		
	}
}