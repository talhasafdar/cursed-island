using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the weapons collected.
*/
public class Weapons : MonoBehaviour
{
	private GameObject playerLoad; // for audio holder since it remains
	[SerializeField] private AudioClip gunObtained;
	public static int countWeapons = 0;  // if 2 then can aim and shoot 
	[SerializeField] private GameObject ammoBox;
	[SerializeField] private GameObject ammoBoxMessage;
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && ammoBoxMessage != null) 
		{
			playerLoad = other.gameObject;
			ammoBoxMessage.SetActive(true);
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (ammoBoxMessage != null)		
		{
			ammoBoxMessage.SetActive(false);
		}
	}
	
	public void CollectWeapon() 
	{
		AudioSource.PlayClipAtPoint(gunObtained, playerLoad.transform.position, MainMenu.sfxVolume);
		Destroy(gameObject);
		Destroy(ammoBoxMessage);
		countWeapons = countWeapons + 1;
	}
}