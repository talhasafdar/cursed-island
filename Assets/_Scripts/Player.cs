using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/*
	This class is used to manage the behaviour of the player.
*/
public class Player : MonoBehaviour
{
	[Header("Audio")]		
	[SerializeField] private AudioSource backgroundMusic;
	[SerializeField] private AudioSource stepsSound;	
	[SerializeField] private AudioSource jump;	
	[SerializeField] private AudioSource beach;	
	[SerializeField] private AudioSource campfire;	
	
	[Header("Cameras")]
	[SerializeField] private Camera normalCam;
	[SerializeField] private Camera shootingCam;
	
	[Header("Player Features")]
	[SerializeField] private CharacterController controller; // motor
	[SerializeField] private Transform cam;
	[SerializeField] private float walkSpeed;		
	[SerializeField] private float runSpeed;				
	[SerializeField] private Transform groundCheck;
	[SerializeField] private float groundDistance = 0.4f;
	[SerializeField] private LayerMask groundMask;
	[SerializeField] private float jumpHeight = 3f;
	[SerializeField] private Vector3 velocity; // for gravity
	[SerializeField] private float turnSmoothTime = 0.1f;	
	[SerializeField] private GameObject canvas;	
	
	public static bool isShooting = false;
	private float gravity = -19.62f;
	private float internalSpeed;
	private bool isGrounded;	
	private Vector3 direction;	
	private bool isAiming;
	private Animator anim;
	private float turnSmoothVelocity;
	
	private void OnEnable() 
	{
		if (backgroundMusic.isPlaying == false) 
		{
			backgroundMusic.volume = MainMenu.musicVolume; // assign global volume level
			backgroundMusic.Play();
		}
		stepsSound.volume = MainMenu.sfxVolume; // assign global volume level
		jump.volume = MainMenu.sfxVolume; // assign global volume level
		beach.volume = MainMenu.sfxVolume; // assign global volume level
		campfire.volume = MainMenu.sfxVolume; // assign global volume level
	}
	
	void Start() 
	{
		stepsSound.volume = MainMenu.sfxVolume; // assign global volume level
		jump.volume = MainMenu.sfxVolume; // assign global volume level
		beach.volume = MainMenu.sfxVolume; // assign global volume level
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;	
		anim = GetComponentInChildren<Animator>();
		backgroundMusic.Play();
		shootingCam.enabled = false; // disable shooting mode
	}
	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "slopeLimit") 
		{
			controller.slopeLimit = 90f;
		}
	}
	
	private void OnTriggerExit(Collider other) 
	{
		if (other.gameObject.tag == "slopeLimit") 
		{
			controller.slopeLimit = 60f;
		}
	}

	void Update()
	{
		if (PauseMenu.pauseGame == true && backgroundMusic.isPlaying == true) 
		{
			backgroundMusic.Pause();
		}
		else if (PauseMenu.pauseGame == false && backgroundMusic.isPlaying == false) 
		{
			backgroundMusic.UnPause();
		}
		if (PauseMenu.pauseGame == true && beach.isPlaying == true) 
		{
			beach.Pause();
		}
		else if (PauseMenu.pauseGame == false && beach.isPlaying == false) 
		{
			beach.UnPause();
		}
		
		isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
		
		if (isGrounded == true && velocity.y < 0) 
		{
			// Debug.Log("isGrounded: " + isGrounded);
			velocity.y = -2f;
		}
		
		float horizontal = Input.GetAxisRaw("Horizontal"); // ad
		float vertical = Input.GetAxisRaw("Vertical"); // ws
		direction = new Vector3(horizontal, 0f, vertical).normalized; // this sorts X Y Z
		
		if (direction.magnitude >= 0.1f) 
		{			
			// print("normal rotate");
			 float targetAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg + cam.eulerAngles.y; // this allowed movement, but avatar don't turn
			 float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, turnSmoothTime);	// calculate angle for avatar turn	
			 transform.rotation = Quaternion.Euler(0f, angle, 0f); // apply new calculated angle
			
			Vector3 moveDir = Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;	// moves ahead
			controller.Move(moveDir.normalized * internalSpeed * Time.deltaTime);	// moves ahead			
		}	
		if (direction != Vector3.zero && isGrounded)			
		{
			// print("walking");
			stepsSound.pitch = 0.75f;
			if (!stepsSound.isPlaying) 
			{
				stepsSound.Play();
			}
			Walk();	
		}
		if(direction != Vector3.zero && Input.GetKey(KeyCode.LeftShift))
		{
			// print("running");
			stepsSound.pitch = 1.5f;
			if (!stepsSound.isPlaying) 
			{
				stepsSound.Play();
			}
			Run();
		}
		else if(direction == Vector3.zero) 
		{
			stepsSound.Stop();	
			Idle();
		}
		
		
		if (Input.GetButtonDown("Jump") && isGrounded == true) 
		{
			if (!jump.isPlaying) 
			{
				jump.Play();
			}
			Jump();				
		}	
		
		if (Weapons.countWeapons == 2) 		
		{	
			if(Input.GetKeyDown(KeyCode.Mouse1) || Input.GetKeyUp(KeyCode.Mouse1))		
			{
				if(Input.GetKeyDown(KeyCode.Mouse1))
				{
					// print("pressed: AIM");
					canvas.SendMessage("ToggleAim", true);
					shootingCam.enabled = true;
					normalCam.enabled = false;
					isAiming = true;
					Aim(true);
				}
				else if(Input.GetKeyUp(KeyCode.Mouse1))
				{
					canvas.SendMessage("ToggleAim", false);
					normalCam.enabled = true;
					shootingCam.enabled = false;
					isAiming = false;
					Aim(false);
				}
			}		
			if(Input.GetKeyDown(KeyCode.Mouse0) && isAiming == true)
			{
				isShooting = true;
				Attack();
			}
			else 
			{
				isShooting = false;
			}
			//print("AIM: can't aim");
		}
				
		velocity.y += gravity* Time.deltaTime; // for gravity
		controller.Move(velocity * Time.deltaTime); // for gravity
	}	
	
	private void Idle() 
	{
		//stepsSound.Stop();
		anim.SetFloat("Speed", 0, 0.1f, Time.deltaTime);
	}
	
	private void Walk() 
	{
		internalSpeed = walkSpeed;
		anim.SetFloat("Speed", 0.5f, 0.1f, Time.deltaTime);
	}
	
	private void Run() 
	{
		internalSpeed = runSpeed;
		anim.SetFloat("Speed", 1, 0.1f, Time.deltaTime);
	}
	private void Jump() 
	{
		anim.SetTrigger("Jump");
		velocity.y = Mathf.Sqrt(jumpHeight * -2f * gravity);
	}
	private void Aim(bool setActive)
	{
		if(setActive == true)
		{
			anim.SetBool("Aiming", true);	
			// print("AIMING");
		}	
		else
		{
			anim.SetBool("Aiming", false);
			// print("NOT AIMING");
		}
	}
	
	private void Attack()
	{
		anim.SetTrigger("Attack");
		// print("ATTACKING");
	}
}