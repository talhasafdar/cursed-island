using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	The class AncientArtefact is used to manage the placing of the artefact
	into the appropriate place.
*/
public class AncientArtefact : MonoBehaviour
{
	[SerializeField] private GameObject artefactMessage; // the hint message [0]
	[SerializeField] private GameObject artefactPlaced; // the artefact game object [1]
	
	/*
		This function detects the player and enables the hint message [0].
	*/
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "Player" && artefactMessage != null) 
		{
			artefactMessage.SetActive(true);
		}
	}
	
	/*
		This function detects the player and disables the hint message [0].
	*/
	private void OnTriggerExit(Collider other) 
	{
		if (artefactMessage != null)		
		{
			artefactMessage.SetActive(false);
		}
	}
	
	/*
		This function destroys the game object and
		enables the artefact game object [1].
	*/
	public void PlaceArtefact() 
	{
		Destroy(artefactMessage);
		artefactPlaced.SetActive(true);
	}
}