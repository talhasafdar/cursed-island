using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/*
	This class is used to manage the lighting effect when
	the ancient artefact is palaced.
*/
public class ArtefactLight : MonoBehaviour
{
	private int animateLight = 0; // start from 0
	private int limit = 60; // limit of the animation
	private bool completed = false; // check if animation is completed
	[SerializeField] private AudioSource soundEffect; // sound for the animation
	
	private void Start() 
	{
		soundEffect.volume = MainMenu.sfxVolume;	
	}
	
	void FixedUpdate()
	{
		if (animateLight <= limit)
		{
			soundEffect.Play();
			transform.Translate(new Vector3(0, 0, -1 * Time.deltaTime)); // move the light perpendicularly upward
			animateLight = animateLight +1;
		}
		else
		{
			completed = true;
		}
		
		if (completed == true) 
		{
			StartCoroutine(EndGame()); // call delayed function
		}
	}
	
	/*
		This function is used to end the game.
	*/
	IEnumerator EndGame()
	{
		//Wait for the specified delay time before continuing.
		yield return new WaitForSeconds(5);
		SceneManager.LoadScene(0); // load the main menu scene
	}
}