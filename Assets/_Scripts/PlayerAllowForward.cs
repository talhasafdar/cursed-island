using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used for the player to allow go forward ONLY if 
	current hint/gun is collected.
*/
public class PlayerAllowForward : MonoBehaviour
{	
	private void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.tag == "AllowForward1") 
		{
			if (PlayerInventory.countHints == 1) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward2") 
		{
			if (PlayerInventory.countGuns == 1) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward3") 
		{
			if (PlayerInventory.countHints == 2 && PlayerInventory.countGuns == 2) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward4") 
		{
			if (PlayerInventory.countHints == 3) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward5") 
		{
			if (PlayerInventory.countHints == 4) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward6") 
		{
			if (PlayerInventory.countHints == 5) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward7") 
		{
			if (PlayerInventory.countHints == 6) 
			{
				Destroy(other.gameObject);
			}
		}
		else if (other.gameObject.tag == "AllowForward8") 
		{
			if (PlayerInventory.countHints == 7) 
			{
				Destroy(other.gameObject);
			}
		}
	}
}