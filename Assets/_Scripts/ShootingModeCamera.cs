using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the camera for the shooting mode.
*/
public class ShootingModeCamera : MonoBehaviour
{
	[SerializeField] private float mouseSensitivity;	
	private Transform parent;
	[SerializeField] private float xRotation = 0f;
	
	void Start()
	{
		parent = transform.parent;
	}

	void Update()
	{
		Rotate();
	}
	
	private void Rotate() 
	{
		float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
		float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;
		xRotation -= mouseY;		
		transform.localRotation = Quaternion.Euler(xRotation, 0f, 0f);
		parent.Rotate(Vector3.up * mouseX);
	}
}