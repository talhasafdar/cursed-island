using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
	This class is used to manage the shooting mode.
*/
public class ShootingMode : MonoBehaviour
{
	private float walkSpeed;
	private Vector3 moveDirection;	
	private CharacterController controller;
	
	private void Start() 
	{
		controller = GetComponent<CharacterController>();
	}
	
	private void Update() 
	{
		Move();
	}
	
	private void Move() 
	{
		float moveZ = Input.GetAxis("Vertical");
		moveDirection = new Vector3(0, 0, moveZ);
		moveDirection = moveDirection * walkSpeed;
		
		controller.Move(moveDirection * Time.deltaTime);
	}
}