# Cursed Island
This project is about creating an action-based adventure game. 


# Additional Information
Please note that due to limited storage resources, only the scripts for this project have been version controlled in this repository. The other assets, such as textures, models, and audio files, are not included. However, the scripts are well-organized and showcase my programming skills and understanding of game development concepts.

## Features
- Virtual Reality
- Immersive Experience

## Contributing and Setup
If you would like to contribute and set up the project with the missing assets, please follow these steps:
1. Contact the author at: [Safdardeveloper@gmail.com](mailto:Safdardeveloper@gmail.com)
2. Obtain the full project files
3. Fetch any changes and start committing
